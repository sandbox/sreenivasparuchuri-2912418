<?php

namespace Drupal\geo_location\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configures Geo API settings for this site.
 */
class ApiSettingsForm extends ConfigFormBase {

  /**
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.   
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'geo_location_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['geo_location.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('geo_location.settings');
    $fields_options = array('country', 'countryCode', 'region', 'regionName', 'city', 'zip', 'lat', 'lon', 'timezone', 'isp', 'org', 'as', 'reverse', 'mobile', 'proxy', 'query', 'status', 'message');
    $fields = $config->get('api_fields');
    foreach ($fields_options as $key => $value) {
      $options[$value] = $this->t(strtoupper($value));
    }
    $form['api_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('API URL'),
      '#default_value' => $config->get('api_url'),
      '#required' => TRUE,
    );
    $form['api_allowed_fields'] = array(
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => !empty($fields) ? $config->get('api_fields') : array('country', 'countryCode', 'region', 'regionName', 'city', 'zip', 'status', 'message'),
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $api_fields = array_filter($form_state->getValue('api_allowed_fields'));
    sort($api_fields);
    $this->config('geo_location.settings')
        ->set('api_fields', $api_fields)
        ->set('api_url', $form_state->getValue('api_url'))
        ->save();

    parent::submitForm($form, $form_state);
  }

}
