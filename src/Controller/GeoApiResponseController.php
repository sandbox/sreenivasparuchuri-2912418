<?php

namespace Drupal\geo_location\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\geo_location\Controller\GeoApiFetch;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormBuilderInterface;

/**
 * Controller for Slider.
 */
class GeoApiResponseController extends ControllerBase {

  /**
   * The Geo Location service.
   *
   * @var \Drupal\geo_location\Controller\GeoApiFetch
   */
  protected $geo_location_service;

  /**
   * The form builder service.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('geolocation.call_api'), $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(GeoApiFetch $geo_api_handler, FormBuilderInterface $form_builder) {
    $this->geo_location_service = $geo_api_handler;
    $this->formBuilder = $form_builder;
  }

  /**
   * Render a list of entries in the database.
   */
  public function GeoApiResponse() {
    $build['geo_api_query_form'] = $this->formBuilder->getForm('Drupal\geo_location\Form\APIQueryForm');
    $build['#markup'] = '<h2>' . $this->t('Sample Geo Location API Response') . '</h2';
    $rows = array();
    $headers = array(t('Name'), t('Value'));
    $query_field = !empty($_SESSION['geo_api_query_field']) ? $_SESSION['geo_api_query_field'] : "";
    $response = $this->geo_location_service->getResponse($query_field);
    foreach ($response as $key => $value) {
      if (!empty($value)) {
        $rows[] = array(
        ucfirst($key), ucfirst($value));
      }      
    }
    $build['table'] = array(
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => t('No entries available.'),
    );
    return $build;
  }

}
