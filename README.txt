INTRODUCTION
------------

The GEO LOCATiON  module gives the ability to get country,countryCode,region, regionName,city,zip,lat,lon,timezone,isp,org,as,reverse,mobile,proxy, values based on given IP or domain name. If then functions 
\Drupal::service('geolocation.call_api')->getCountryCode() 
\Drupal::service('geolocation.call_api')->getCountry() were called with empty parameter it will give country code based on user client IP.

We can also call service with query field like below. Where drupal.org is an input to API. We can pass both IP Address and Domain Names.

\Drupal::service('geolocation.call_api')->getCountryCode('drupal.org')


MAINTAINERS
-----------

Current maintainers:
 * Gabriel Machado Santos - https://www.drupal.org/u/sreenivasparuchuri