<?php

namespace Drupal\geo_location\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\Client;

class GeoApiFetch {

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a StaticMenuLinkOverrides object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A configuration factory instance.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  public function getCountryCode($query = '') {
    $response = $this->getResponse($query);
    return isset($response['countryCode']) ? $response['countryCode'] : '';
  }

  public function getCountry($query = '') {
    $response = $this->getResponse($query);
    return isset($response['country']) ? $response['country'] : '';
  }

  public function getRegion($query = '') {
    $response = $this->getResponse($query);
    return isset($response['region']) ? $response['region'] : '';
  }

  public function getCity($query = '') {
    $response = $this->getResponse($query);
    return isset($response['city']) ? $response['city'] : '';
  }

  public function getTimezone($query = '') {
    $response = $this->getResponse($query);
    return isset($response['timezone']) ? $response['timezone'] : '';
  }

  public function getZip($query = '') {
    $response = $this->getResponse($query);
    return isset($response['zip']) ? $response['zip'] : '';
  }

  public function getLattitude($query = '') {
    $response = $this->getResponse($query);
    return isset($response['lat']) ? $response['lat'] : '';
  }

  public function getLognitude($query = '') {
    $response = $this->getResponse($query);
    return isset($response['lon']) ? $response['lon'] : '';
  }

  public function isMobileConnection($query = '') {
    $response = $this->getResponse($query);
    return isset($response['mobile']) ? $response['mobile'] : '';
  }

  public function isproxy($query = '') {
    $response = $this->getResponse($query);
    return isset($response['proxy']) ? $response['proxy'] : '';
  }

  public function getReverseDNS($query = '') {
    $response = $this->getResponse($query);
    return isset($response['reverse']) ? $response['reverse'] : '';
  }

  public function getOrginization($query = '') {
    $response = $this->getResponse($query);
    return isset($response['org']) ? $response['org'] : '';
  }

  public function getResponse($query = '') {
    $config = $this->configFactory->get('geo_location.settings');
    $fields = implode(',', $config->get('api_fields'));
    $data = $this->CallGeoLocationApi($query, $fields);

    if (isset($data['status']) && $data['status'] = 'fail' && isset($data['message'])) {
      $data = array('message' => t('Geo Location API not able to fetch due to :message', array(':message' => $data['message'])));
      return $data;
    }
    return $data;
  }

  private function CallGeoLocationApi($query = '', $fields) {
    $config = $this->configFactory->get('geo_location.settings');
    $url = $config->get('api_url');
    if (empty($query)) {
      $query = \Drupal::request()->getClientIP();
    }
    if (is_callable('curl_init')) {
      $c = curl_init();
      curl_setopt($c, CURLOPT_URL, $url . $query . '?fields=' . $fields);
      curl_setopt($c, CURLOPT_HEADER, false);
      curl_setopt($c, CURLOPT_TIMEOUT, 30);
      curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
      $response = unserialize(curl_exec($c));
      curl_close($c);
    }
    else {
      $response = unserialize(file_get_contents($url . $query . '?fields=' . $fields));
    }

    return $response;
  }

}
