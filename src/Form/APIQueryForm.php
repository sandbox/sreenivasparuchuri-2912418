<?php

namespace Drupal\geo_location\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Query Field Form.
 */
class APIQueryForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'geo_location_response_verify_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['query_field'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Query Field'),
      '#description' => $this->t('This field will be passed to API as query paramater. You can enter either IP Address or Domain name'),
      '#default_value' => !empty($_SESSION['geo_api_query_field']) ? $_SESSION['geo_api_query_field'] : "",
      '#required' => TRUE,
    );
    $form['filters']['actions'] = array(
      '#type' => 'actions',
      '#attributes' => array('class' => array('container-inline')),
    );
    $form['filters']['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    );
    if (!empty($_SESSION['geo_api_query_field'])) {
      $form['filters']['actions']['reset'] = array(
        '#type' => 'submit',
        '#value' => $this->t('Reset'),
        '#limit_validation_errors' => array(),
        '#submit' => array('::resetForm'),
      );
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $matches = array();
    if ($form_state->isValueEmpty('query_field')) {
      $form_state->setErrorByName('query_field', $this->t('You must enter either IP Address or Domain Name to see the API reponse.'));
    }
    else if (preg_match("/(http|https):\/\/(.*?)$/i", $form_state->getValue('query_field'), $matches) > 0) {
      $form_state->setValue('query_field', $matches[2]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->hasValue('query_field')) {
      $_SESSION['geo_api_query_field'] = $form_state->getValue('query_field');
    }
  }

  /**
   * Resets the filter form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function resetForm(array &$form, FormStateInterface $form_state) {
    $_SESSION['geo_api_query_field'] = array();
  }

}
